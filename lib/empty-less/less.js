//>>excludeStart('require', pragmas.require);
define(['require'], function(require) {
    "use strict";
    var lessAPI = {};

    lessAPI.normalize = function(name, normalize) {
        if (name.substr(name.length - 5, 5) === '.less') {
            name = name.substr(0, name.length - 5);
        }

        name = normalize(name);

        return name;
    };

    lessAPI.inject = function() {
    };

    lessAPI.load = function(lessId, req, load, config) {
        load();
    };

    return lessAPI;
});
//>>excludeEnd('require');