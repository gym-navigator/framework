(function (document, Fw) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "./core",
        "./debug"
    ], function (Fw) {
        //>>excludeEnd('require');
        var remote = {
            getJSON:function (url, data, callback, protocol) {
                var r = new XMLHttpRequest(),
                    sdata = new FormData(),
                    i,j;

                if (data instanceof FormData) {
                    sdata = data;
                }
                else {
                    if (protocol==='POST') {
                    for (i in data) {
                        if (typeof data[i] === 'object') {
                            for (j in data[i]) {
                                sdata.append(i+'['+j+']', data[i][j]);
                            }
                        } else {
                           sdata.append(i, data[i]);
                        }
                    }
                    }
                        else {
                            for (i in data) {
                                if (typeof data[i] === 'object') {
                                    for (j in data[i]) {
                                        url += i+'['+j+']=' + data[i][j] + '&';
                                    }
                                } else {
                                    url += i + '=' + data[i]+'&';
                                }
                            }
                        }

                }
                r.open(protocol || "POST", url, true);
                r.timeout = 5000;
                r.onreadystatechange = function () {
                    var responseData;
                    Fw.log(r.readyState, r.status);
                    if (r.readyState !== 4 || r.status !== 200) {
                        return;
                    }
                    try {
                        responseData = JSON.parse(r.responseText);
                    }
                    catch (e) {
                        responseData = {};
                    }
                    callback(responseData);
                };
                r.send(sdata);

            },
            getText:function (url, data, callback, errorCallback, protocol) {
                var r = new XMLHttpRequest(),
                    sdata = new FormData();

                if (data instanceof FormData) {
                    sdata = data;
                }
                r.open(protocol || "POST", url, true);
                r.onreadystatechange = function () {
                    var responseData = r.responseText;
                    if (r.readyState !== 4) {
                        return;
                    }
                    if (r.status === 200) {
                        callback(responseData);
                    }
                    else {
                        errorCallback(responseData);
                    }
                };
                r.timeout = 5000;
                r.send(sdata);

            },
            loadScript:function (url, onloadCB) {
                var script = document.createElement('script');
                if (onloadCB !== undefined) {
                    script.onload = onloadCB;
                }
                script.src = url;
                document.body.appendChild(script);
            }
        };
        Fw.remote = remote;
        //>>excludeStart('require', pragmas.require);
    });
//>>excludeEnd('require');
}(document, window.Fw));