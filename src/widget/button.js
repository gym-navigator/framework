//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    '../api/google/analytics/request',
    "./label",
    'less!../less/widget/button.less'
], function (widget, Base, analytics) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget, Base, analytics, Label) {
        var Button = function (options) {
            this.options = {
                parent:options.parent,
                text:options.text,
                id:options.id || options.name,
                icon:options.icon,
                position:options.position,
                label:options.label
            };
            this._ui = {};
            this.create();
            this.generateId(this.options.id);
            this.setParent(this.options.parent);
            if (options.label) {
                this._ui.label = new Label({reference:this, text:options.label});
            }
            this.bindEvents();
        };

        Button.prototype = new Base();

        Button.prototype.create = function (parent) {
            var ui = this._ui,
                options = this.options,
                container,
                text,
                icon;
            container = document.createElement('div');
            container.classList.add('ui-button');
            if (options.position) {
                container.classList.add('ui-button-position-' + options.position);
            }
            if (options.icon) {
                icon = document.createElement('div');
                icon.classList.add('ui-button-icon');
                icon.classList.add('ui-button-' + options.icon);
                container.appendChild(icon);
                ui.icon = icon;
            }
            if (options.text) {
                text = document.createElement('div');
                text.classList.add('ui-button-inner');
                text.innerText = options.text;
                container.appendChild(text);
                ui.text = text;
            }
            ui.container = container;
        };

        Button.prototype.bindEvents = function () {
            var container = this._ui.container,
                containerClassList = container.classList,
                self = this;
            container.addEventListener('mousedown', function () {
                containerClassList.add('ui-button-down');
            });
            container.addEventListener('mouseup', function () {
                containerClassList.remove('ui-button-down');
            });
            container.addEventListener('mouseover', function () {
                containerClassList.add('ui-button-over');
            });
            container.addEventListener('mouseout', function () {
                containerClassList.remove('ui-button-over');
            });
            container.addEventListener('click', function() {
                analytics.event({
                    category: 'button',
                    action: 'click',
                    label: this.id || ''
                });
                if (self.onclick) {
                    self.onclick();
                }
            });
        };

        Button.prototype.getContainer = function () {
            if (this._ui.label !== undefined) {
                return this._ui.label.getContainer();
            }
            return this._ui.container;
        };

        widget.Button = Button;
    }(document,
        window.Fw.widget,
        window.Fw.widget.Base,
        window.Fw.api.google.analytics.request,
        window.Fw.widget.Label
    ));
    //>>excludeStart('require', pragmas.require);
    return widget.Button;
});
//>>excludeEnd('require');