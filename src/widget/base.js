//>>excludeStart('require', pragmas.require);
define([
    "../widget"
], function (widget) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, widget) {
        var Base = function () {
            },
            widgets = [],
            lastId = 0;

        Base.find = function (id) {
            return widgets[id];
        }

        Base.prototype.setParent = function (parent) {
            if (parent) {
                parent.appendChild(this._ui.container);
            }
        };

        Base.prototype.generateId = function (id) {
            var newId = id || this.getContainer().id || 'widget-' + lastId;
            lastId++;
            this.setId(newId);
        };

        Base.prototype.setId = function (id) {
            this.id = id;
            this.getContainer().id = id;
        };

        Base.prototype.getId = function () {
            return this.id;
        };

        Base.prototype.register = function (id) {
            this.generateId(id);
            widgets[this.id] = this;
        };

        Base.prototype.getContainer = function () {
            if (this._ui.label !== undefined) {
                return this._ui.label.getContainer();
            }
            return this._ui.container;
        };

        Base.prototype.hide = function () {
            var container = this.getContainer();
            container.style.display = 'none';
        };

        Base.prototype.show = function () {
            var container = this.getContainer();
            container.style.display = 'block';
        };


        widget.Base = Base;
    }(document, window.Fw.widget));
    //>>excludeStart('require', pragmas.require);
    return widget.Base;
});
//>>excludeEnd('require');
