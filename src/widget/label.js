//>>excludeStart('require', pragmas.require);
define([
    "../widget",
    "./base",
    'less!../less/widget/label.less'
], function (widget, Base) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, widget, Base) {

        var Label = function (options) {
            this.options = {
                reference:options.reference,
                text:options.text
            };
            this._ui = {};
            this.create(this.options.reference);
        };

        Label.prototype = new Base();

        Label.prototype.create = function (reference) {
            var ui = this._ui,
                referenceContainer,
                referenceContainerParentNode,
                container,
                label;

            referenceContainer = reference && reference.getContainer(),
            referenceContainerParentNode = referenceContainer && referenceContainer.parentNode,
            container = document.createElement('div');
            label = document.createElement('label');
            label.innerHTML = this.options.text;
            label.classList.add('ui-input-text');
            container.appendChild(label);
            if (referenceContainerParentNode) {
                referenceContainerParentNode.insertBefore(container, referenceContainer);
            }
            if (referenceContainer) {
                container.appendChild(referenceContainer);
            }
            ui.container = container;
            ui.label = label;
        };

        Label.prototype.getContainer = function () {
            return this._ui.container;
        };

        widget.Label = Label;
    }(document, window.Fw.widget, window.Fw.widget.Base));
    //>>excludeStart('require', pragmas.require);
    return widget.Label;
});
//>>excludeEnd('require');
