(function (document, widget, Base) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../widget",
        "./base",
        'less!../less/widget/table.less'
    ], function (widget) {
        //>>excludeEnd('require');

        var Table = function (options) {
            this.options = {
                parent:options.parent,
                columns:options.columns
            };
            this._ui = {};
            this.create();
            this.bindEvents();
            this.setParent(this.options.parent);
        };

        Table.prototype = new Base();

        Table.prototype.create = function (parent) {
            var ui = this._ui,
                i,
                cell,
                row,
                columns = this.options.columns;
            ui.container = document.createElement('table');
            ui.container.classList.add('ui-table');
            row = document.createElement('tr');
            row.classList.add('ui-table-header');
            for (i = 0; i < columns.length; i++) {
                cell = document.createElement('th');
                cell.classList.add('ui-table-header-cell');
                cell.innerText = columns[i];
                row.appendChild(cell);
            }
            ui.container.appendChild(row);
        };

        Table.prototype.showData = function (data, columns) {
            var ui = this._ui,
                i,
                j,
                cell,
                row,
                columns = this.options.columns;
            for (j = 0; j < data.length; j++) {
                row = document.createElement('tr');
                row.classList.add('ui-table-row');
                row.dataset.index = j;
                for (i = 0; i < columns.length; i++) {
                    cell = document.createElement('td');
                    cell.classList.add('ui-table-cell');
                    cell.innerText = data[j][columns[i]] || '';
                    row.appendChild(cell);
                }
                ui.container.appendChild(row);
            }
        };

        Table.prototype.bindEvents = function () {
            var self = this;
            this._ui.container.addEventListener('click', function(event) {
                var index,
                    target = event.target;
                while (target && !target.classList.contains('ui-table-row')) {
                    target = target.parentNode;
                }
                index = parseInt(target.dataset.index, 10);
                if (self.onclick) {
                    self.onclick(index);
                }
            });
        };

        Table.prototype.getContainer = function () {
            return this._ui.container;
        };

        widget.Table = Table;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(document, window.Fw.widget, window.Fw.widget.Base));