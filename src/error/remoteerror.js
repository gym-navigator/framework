//>>excludeStart('require', pragmas.require);
define([
    "../error",
    "./base",
    "../remote"
], function (error, Base, remote) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, error, Base, remote, GNversion) {
        var RemoteError = function () {
        };

        RemoteError.prototype = new Base();

        RemoteError.prototype.error = function (errorMsg, url, lineNumber, columnNumber, errorObj) {
            remote.getJSON('http://gymnavigator.com/app_dev.php/api/REST/error',
                {
                    summary: errorMsg,
                    file: url,
                    code:errorMsg + ' (' + (url || 0) + ":" + (lineNumber || 0) + ':' + (columnNumber || 0) + ')' + "\n" +
                        (errorObj && errorObj.stack || ''),
                    type: 'bug',
                    referrer: (location && location.href || ''),
                    useragent: (window && window.navigator && window.navigator.userAgent || ''),
                    priority: 0,
                    line: lineNumber,
                    version: __version__,
                    build: __build__
                }, function () {
                    console.log('Successfully submitted ticket!'); // You might want to redirect the user here, or show a stylized message to them.
                }, 'POST');
            return false;
        };

        RemoteError.prototype.info = function (content, name, email) {
            remote.getJSON('http://gymnavigator.com/app_dev.php/api/REST/error',
                {
                    summary: content.substr(0, 255),
                    file: '',
                    content: content,
                    type: 'user',
                    priority: 1,
                    reporterName: name,
                    reporterEmail: email,
                    referrer: (location && location.href || ''),
                    useragent: (window && window.navigator && window.navigator.userAgent || ''),
                    version: __version__,
                    build: __build__
                }, function () {
                    console.log('Successfully submitted ticket!'); // You might want to redirect the user here, or show a stylized message to them.
                }, 'POST');
            return false;
        };

        error.RemoteError = RemoteError;
    }(document, window.Fw.error,
        window.Fw.error.Base,
        window.Fw.remote,
        '0.1.2'
    ));
    //>>excludeStart('require', pragmas.require);
    return error.RemoteError;
});
//>>excludeEnd('require');
