//>>excludeStart('require', pragmas.require);
define([
    "../core",
    "../api",
    'fw/error/remoteerror'
], function () {
    "use strict";
    //>>excludeEnd('require');
    (function (Fw, api, RemoteError) {
        var remoteError = new RemoteError(),
            facebook = {
            initFB : function(appId, callback) {
                // init the FB JS SDK
                FB.init({
                    appId      : appId                        // App ID from the app dashboard
                    //>>excludeStart('require', !pragmas.cordova);
                    ,status     : true,                                 // Check Facebook Login status
                    xfbml      : true                                  // Look for social plugins on the page
                    //>>excludeEnd('require');
                    //>>excludeStart('require', pragmas.cordova);
                    ,nativeInterface: CDV.FB,
                    useCachedDialogs: false
                    //>>excludeEnd('require');
                });
                if (callback) {
                    callback();
                }
            },
            init : function (appId, callback) {
                var fbDiv = document.createElement('div'),
                    js = document.createElement('script'),
                    body = document.body;
                fbDiv.id = 'fb-root';
                body.appendChild(fbDiv);
                if (window.FB) {
                    document.addEventListener('deviceready', function() {
                    facebook.initFB(appId, callback);
                    });
                } else {
                    window.fbAsyncInit = facebook.initFB.bind(facebook, appId, callback);

                    js.src = "//connect.facebook.net/en_US/all.js";
                    body.appendChild(js);
                }
            },
            login: function(options, callback) {
                window.FB.getLoginStatus(facebook.checkLogin.bind(facebook, options, callback));
            },
            checkLogin: function(options, callback, response) {
                if (response.status === 'connected') {
                    callback(response);
                } else {
                    window.FB.login(facebook.checkLogin.bind(facebook, options, callback), options);
                }
            },
            getUser: function (callback) {
                window.FB.api('/me', function(response) {
                    remoteError.info('facebook getuser response', response, 'proposal');
                    callback(response);
                });
            }
        };
        api.facebook = facebook;
    }(window.Fw, window.Fw.api,window.Fw.error.RemoteError));
        //>>excludeStart('require', pragmas.require);
    });
//>>excludeEnd('require');