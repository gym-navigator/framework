(function (google) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../google"
    ], function () {
        //>>excludeEnd('require');
        google.analytics = google.analytics || {};
        //>>excludeStart('require', pragmas.require);
        return google.analytics;
    });
//>>excludeEnd('require');
}(window.Fw.api.google));