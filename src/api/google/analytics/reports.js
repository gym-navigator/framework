(function (analytics) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../analytics"
    ], function () {
        //>>excludeEnd('require');
        var Reports = function (options) {
            this.options = {
                clientId: options.clientId,
                apiKey: options.apiKey,
                scopes: options.scopes,
                accountId: options.accountId,
                webPropertyId: options.webPropertyId,
                profileId: options.profileId
            };
            this.handleAuthorized = options.handleAuthorized;
            this.data = {};
        };

        Reports.prototype.authorize = function() {
            gapi.client.setApiKey(this.options.apiKey);
            gapi.auth.authorize({client_id:this.options.clientId, scope:this.options.scopes, immediate:true}, this.handleAuthResult.bind(this));
        };

        Reports.prototype.handleAuthResult = function(authResult) {
            var self = this;
            this.authResult = authResult;
            if (authResult) {
                gapi.client.load('analytics', 'v3', function () {
                    if (self.handleAuthorized !== undefined) {
                        self.handleAuthorized();
                    }
                });
            }
        };

        Reports.prototype.queryAccounts = function() {
            gapi.client.analytics.management.accounts.list().execute(this.handleAccounts.bind(this));
        };

        Reports.prototype.handleAccounts = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.accounts = results.items;
                }
            }
        };

        Reports.prototype.queryWebProperties = function(accountId) {
            accountId = accountId || this.options.accountId || this.data.accounts[0].id;
            gapi.client.analytics.management.webproperties.list({
                'accountId':accountId
            }).execute(this.handleWebProperties.bind(this));
        };

        Reports.prototype.handleWebProperties = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.webProperties = results.items;
                }
            }
        };

        Reports.prototype.queryProfiles = function(accountId, webPropertyId) {
            accountId = accountId || this.options.accountId || this.data.accounts[0].id;
            webPropertyId = webPropertyId || this.options.webpropertyId || this.data.webProperties[0].id;
            gapi.client.analytics.management.profiles.list({
                'accountId':accountId,
                'webPropertyId':webPropertyId
            }).execute(this.handleProfiles.bind(this));
        };

        Reports.prototype.handleProfiles = function(results) {
            if (!results.code) {
                if (results.items && results.items.length) {
                    this.data.profiles = results.items;
                }
            }
        };

        Reports.prototype.query = function(options, handle) {
            if (!options.ids) {
                options.ids = 'ga:' + this.options.profileId;
            }
            gapi.client.analytics.data.ga.get(options).execute(handle);
        };

        analytics.Reports = Reports;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(window.Fw.api.google.analytics));