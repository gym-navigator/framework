//>>excludeStart('require', pragmas.require);
define([
    "../layout",
    "../widget/button",
    "../layout/base",
    '../api/google/ads',
    'less!../less/layout/page.less'
], function (layout, Button, Base, ads) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, layout, Button, Base, ads) {
        var Page = function (options) {
            this.options = {
                parent:options.parent,
                fixed:options.fixed || true,
                theme:options.theme || 'c',
                adv:options.adv !== undefined ? options.adv : true
            };
            this._ui = {
            };
        };

        Page.prototype = new Base();

        Page._ui = {};

        Page.prototype.build = function () {
            this.create(this.options.parent || document.getElementById('container') || document.body);
            this.setTheme(this.options.theme);
            this.setFixed(this.options.fixed);
        };

        Page.prototype.create = function (parent) {
            var ui = this._ui;

            ui.container = document.createElement('div');
            ui.container.classList.add('ui-page');
            ui.header = document.createElement('div');
            ui.header.classList.add('ui-header');
            ui.headerText = document.createElement('div');
            ui.headerText.classList.add('ui-header-text');
            ui.content = document.createElement('div');
            ui.content.classList.add('ui-content');
            ui.footer = document.createElement('div');
            ui.footer.classList.add('ui-footer');
            ui.header.appendChild(ui.headerText);
            ui.container.appendChild(ui.header);
            ui.container.appendChild(ui.content);
            ui.container.appendChild(ui.footer);
            if (!Page._ui.adv && !window.cordova) {
                Page._ui.adv = document.createElement('div');
                Page._ui.advInner = document.createElement('div');
                Page._ui.adv.appendChild(Page._ui.advInner);
                Page._ui.advInner.classList.add('ui-adv-inner');
                parent.appendChild(Page._ui.adv);
                //>>excludeStart('require', !pragmas.fb);
                Page._ui.adv.classList.add('ui-adv');
                //>>excludeEnd('require');
                //>>excludeStart('require', pragmas.fb);
                Page._ui.adv.classList.add('ui-adv-fb');
                //>>excludeEnd('require');

            }
            if (this.options.adv && window.innerWidth > 640 && !window.cordova) {
                //>>excludeStart('require', !pragmas.fb);
                ui.container.classList.add('ui-page-adv');
                //>>excludeEnd('require');
                //>>excludeStart('require', pragmas.fb);
                ui.container.classList.add('ui-page-adv-fb');
                //>>excludeEnd('require');
            }
            parent.appendChild(ui.container);
            //contentClassList.add('sport_workout_list ui-page ui-body-c ui-page-header-fixed ui-page-footer-fixed ui-page-active');
        };

        Page.prototype.reloadAds = function () {
            if (!window.cordova) {
                var count = 0,
                    intervalId,
                    interval = function () {
                        if (count < 5) {
                            Page._ui.advInner.innerHTML = window.App.adCode;
                            setTimeout(function () {
                                ads.show();
                            }, 1000);
                            count++;
                        } else {
                            clearInterval(interval);
                        }
                    };
                intervalId = setInterval(interval, 120000);
                interval();
            }
        };

        Page.prototype.setActive = function (value) {
            var contentClassList = this._ui.container.classList;
            if (value === true) {
                contentClassList.add('ui-page-active');
            }
            else {
                contentClassList.remove('ui-page-active');
            }
        };

        Page.prototype.setTheme = function (value) {
            var contentClassList = this._ui.container.classList;
            contentClassList.add('ui-body-' + value);
        };

        Page.prototype.setTitle = function (value) {
            this._ui.headerText.innerHTML = value;
        };

        Page.prototype.setContent = function (value) {
            if (value instanceof HTMLElement) {
                this._ui.content.innerHTML = '';
                this._ui.content.appendChild(value);
            }
            else {
                this._ui.content.innerHTML = value;
            }
        };

        Page.prototype.getHeader = function () {
            return this._ui.header;
        };

        Page.prototype.getContent = function () {
            return this._ui.content;
        };

        Page.prototype.getFooter = function () {
            return this._ui.footer;
        };

        Page.prototype.setFixed = function (value) {
            var contentClassList = this._ui.container.classList,
                headerClassList = this._ui.header.classList,
                footerClassList = this._ui.footer.classList;
            if (value === true) {
                contentClassList.add('ui-page-header-fixed');
                contentClassList.add('ui-page-footer-fixed');
                headerClassList.add('ui-header-fixed');
                footerClassList.add('ui-footer-fixed');
            }
            else {
                contentClassList.remove('ui-page-header-fixed');
                contentClassList.remove('ui-page-footer-fixed');
                headerClassList.remove('ui-header-fixed');
                footerClassList.remove('ui-footer-fixed');
            }
        };

        Page.prototype.addHomeButton = function () {
            this._ui.header_home = new Button({parent:this._ui.header, icon:'home', position:'left'});
            this._ui.header.style.paddingLeft = '19mm';
        };

        Page.prototype.getHomeButton = function () {
            return this._ui.header_home;
        };

        Page.prototype.addNextButton = function (icon) {
            this._ui.header_next = new Button({parent:this._ui.header, icon:icon || 'next', position:'right'});
        };

        Page.prototype.getNextButton = function () {
            return this._ui.header_next;
        };

        layout.Page = Page;
    }(document,
        window.Fw.layout,
        window.Fw.widget.Button,
        window.Fw.layout.Base,
        window.Fw.api.google.ads
    ));
    //>>excludeStart('require', pragmas.require);
});
//>>excludeEnd('require');
