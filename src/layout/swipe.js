//>>excludeStart('require', pragmas.require);
define([
    "../layout",
    "./base",
    'less!../less/layout/swipe.less',
    "./console"
], function (layout, LayoutBase, console) {
    "use strict";
    //>>excludeEnd('require');

    (function (document, layout, LayoutBase, console) {
        var Swipe = function (options) {
            this.options = {
                parent:options.parent,
                heigth:options.heigth || '100%'
            };
            this._ui = {
                elements:[]
            };
            this.create();
            this.setParent(options.parent);

            this.count = 0;
            this.elements = [];
        };

        Swipe.prototype = new LayoutBase();


        Swipe.prototype.create = function () {
            var ui = this._ui,
                self = this,
                width,
                height,
                touchedElement,
                maxMarginY,
                maxMarginX,
                initMarginTop = 0,
                initMarginLeft = 0,
                containerInnerStyle,
                elementwidth;
            ui.container = document.createElement('div');
            ui.container.classList.add('ui-swipe');
            ui.containerInner = document.createElement('div');
            ui.containerInner.classList.add('ui-swipe-inner');
            containerInnerStyle = ui.containerInner.style;
            ui.container.appendChild(ui.containerInner);

            function mousedown(event) {
                var pageX = event.touches !== undefined ? event.touches[0].pageX : event.pageX,
                    pageY = event.touches !== undefined ? event.touches[0].pageY : event.pageY,
                    style,
                    transform;
                self.click = true;
                self.moves = 0;
                height = ui.containerInner.offsetHeight;
                width = ui.container.parentNode.offsetWidth;
                elementwidth = ui.containerInner.offsetWidth / ui.containerInner.children.length;

                containerInnerStyle.webkitTransition = '';

                touchedElement = event.target;
                if (touchedElement !== document) {
                    while (touchedElement.classList && !touchedElement.classList.contains('ui-swipe-element-inner')) {
                        touchedElement = touchedElement.parentNode;
                    }
                }
                if (!(touchedElement instanceof HTMLElement)) {
                    touchedElement = null;
                }
                if (touchedElement) {
                    initMarginTop = parseInt(touchedElement.dataset.top, 10) || 0;
                    maxMarginY = touchedElement.offsetHeight - height;
                }
                self.lastX = pageX;
                self.lastY = pageY;

                maxMarginX = ui.containerInner.offsetWidth - width;
                console.log(event.type, initMarginTop, initMarginLeft, maxMarginX, maxMarginY, self.lastX, self.lastY);
            }

            function mouseup(event) {
                var index = 1.0 * initMarginLeft / elementwidth,
                    time = index - Math.floor(index) - 0.5;
                if (time < 0) {
                    index = Math.floor(index);
                }
                else {
                    index = Math.floor(index) + 1;
                }
                if (touchedElement) {
                    touchedElement.dataset.top = initMarginTop;
                }
                initMarginLeft = index * elementwidth;
                containerInnerStyle.transition = 'all ' + (0.5 - Math.abs(time)) + 's ease-in-out';
                containerInnerStyle.webkitTransform = 'translate(' + initMarginLeft + 'px, 0)';
                self.click = false;
                console.log(event.type, initMarginLeft, elementwidth);
            }

            function mousemove(event) {
                var pageX = event.touches !== undefined ? event.touches[0].pageX : event.pageX,
                    pageY = event.touches !== undefined ? event.touches[0].pageY : event.pageY,
                    moveX = pageX - self.lastX,
                    moveY = pageY - self.lastY;
                self.lastX = pageX;
                self.lastY = pageY;
                event.preventDefault();
                if (touchedElement && self.click && moveY) {
                    initMarginTop = (initMarginTop + moveY );
                    if (initMarginTop > 0) {
                        initMarginTop = 0;
                    }
                    if (-initMarginTop > maxMarginY) {
                        initMarginTop = -maxMarginY;
                    }
                    touchedElement.style.webkitTransform = 'translate(0, ' + initMarginTop + 'px)';
                }
                if (self.click && moveX) {
                    initMarginLeft = (initMarginLeft + moveX );
                    if (initMarginLeft > 0) {
                        initMarginLeft = 0;
                    }
                    if (-initMarginLeft > maxMarginX) {
                        initMarginLeft = -maxMarginX;
                    }
                    containerInnerStyle.webkitTransform = 'translate(' + initMarginLeft + 'px, 0)';
                }
            }
/*
            ui.container.addEventListener('mousedown', mousedown);
            ui.container.addEventListener('mouseup', mouseup);
            ui.container.addEventListener('mousemove', mousemove);

            ui.container.addEventListener('touchstart', mousedown);
            ui.container.addEventListener('touchend', mouseup);
            ui.container.addEventListener('touchmove', mousemove);
            */
        };

        Swipe.prototype.clear = function () {
            var element;
            while (this._ui.elements.length) {
                element = this._ui.elements.pop();
                element.parentNode.removeChild(element);
            }
        };

        Swipe.prototype.add = function (element, colspan) {
            var newElement = document.createElement('div'),
                newElementInner = document.createElement('div');
            newElement.classList.add('ui-swipe-element');
            newElementInner.classList.add('ui-swipe-element-inner');
            colspan = colspan || 1;
            this.count += colspan;
            if (typeof element === 'string') {
                newElementInner.innerHTML = element;
            }
            else {
                newElementInner.appendChild(element.getContainer());
            }
            newElement.dataset.colspan = colspan;
            newElement.appendChild(newElementInner);
            this._ui.containerInner.appendChild(newElement);
            this._ui.elements.push(newElement);
            this.elements.push(element);
        };

        layout.Swipe = Swipe;
    }(document, window.Fw.layout, window.Fw.layout.Base, window.Fw.layout.console));
    //>>excludeStart('require', pragmas.require);
    return layout.Swipe;
});
//>>excludeEnd('require');
