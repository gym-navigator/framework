(function (document, layout) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "../layout"
    ], function () {
        //>>excludeEnd('require');

        var Element = function (options) {
            this.options = {
                parent:options.parent,
            };
            this._ui = {};
            this.create(this.options.parent);
        };

        Element.prototype.create = function (parent) {
            var ui = this._ui;
            ui.container = document.createElement('div');
            parent.appendChild(ui.container);
        };

        Element.prototype.getContainer = function () {
            return this._ui.container;
        };

        layout.Element = Element;
        //>>excludeStart('require', pragmas.require);
    });
    //>>excludeEnd('require');
}(document, window.Fw.layout));