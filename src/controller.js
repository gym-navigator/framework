(function (Fw) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "./core"
    ], function (Fw) {
        //>>excludeEnd('require');
        Fw.controller = Fw.controller || {};
        //>>excludeStart('require', pragmas.require);
        return Fw.controller;
    });
//>>excludeEnd('require');
}(window.Fw));