//>>excludeStart('require', pragmas.require);
define([
    "../model",
    "./object",
    "../widget/base"
], function (model, ModelObject, BaseWidget) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, model, ModelObject, BaseWidget) {
        var BaseModel = function () {
            },
            Base = function (namespace, fields) {
                var Model = function (data) {
                    this.data = data;
                }, i;
                Model.namespace = namespace;
                Model.fields = fields;
                Model.prototype = new ModelObject(Model);
                for (i in BaseModel.prototype) {
                    Model[i] = BaseModel.prototype[i];
                }
                return Model;
            },
            dataAll = {};

        BaseModel.prototype.findAll = function (params, success) {
            var This = this;
            return this.localStore(function (todos) {
                var instances = [],
                    match;
                for (var id in todos) {
                    match = true;
                    for (var key in params) {
                        if (typeof params[key] == 'object') {
                            if (todos[id] && todos[id][key] && todos[id][key].get) {
                                if (params[key].get('id') !== todos[id][key].get('id')) {
                                    match = false;
                                }
                            } else {
                                match = false;
                            }
                        }
                    }
                    if (todos[id] && todos[id].flag !== 'D' && match) {
                        instances.push(new This(todos[id]));
                    }
                    else if (!params) {
                        instances.push(null);
                    }
                }
                if (success) {
                    success(instances);
                }
                return false;
            });
        };

        BaseModel.prototype.setReference = function (data) {
            var type = data && data.get && data.get('type');
            if (type) {
                data = '#ref#' + type + '#' + data.get('id');
            }

            return data;
        };

        BaseModel.prototype.resolveReference = function (data) {
            var refInfo = '', _data,
                dataString = data.toString();
            if (data && dataString.substring(0, 4) === '#ref') {
                refInfo = dataString.split('#');
                if (refInfo[2] && window.App.model[refInfo[2]]) {
                    _data = window.App.model[refInfo[2]].find(refInfo[3]);
                    data = _data;
                } else {
                    data = null;
                }
            }
            return data;
        };

        BaseModel.prototype.find = function (id) {
            var localData = this.localStore(),
                ThisClass = this;
            return new ThisClass(localData[id]);
        };

        BaseModel.prototype.localStore = function (cb, _name, write) {
            var name = this.namespace, data, res, i, j, k,
                _data = [],
                dataI,
                dataIJ;
            if (_name) {
                name = _name;
            }

            if (!write && dataAll[name] !== undefined && !cb) {
                return dataAll[name];
            }

            data = JSON.parse(window.localStorage[name] || (window.localStorage[name] = "[]"));

            if (!data) {
                data = [];
            }

            data = resolveReference(data);

            for (i in data) {
                _data[i] = data[i];
            }

            data = _data;

            if (cb !== undefined) {
                res = cb.call(this, data);
                if (res !== false) {
                    setReference(data);
                    window.localStorage[name] = JSON.stringify(data);
                }
            }
            if (!write) {
                dataAll[name] = data;
            }
            return data;
        };

        function resolveReference(data) {
            var i,
                dataI,
                refInfo = '',
                _data,
                dataString;
            for (i in data) {
                dataI = data[i];
                if (dataI) {
                    if (typeof dataI === 'string') {
                        if (dataI.substring(0, 4) === '#ref') {
                            refInfo = dataI.split('#');
                            if (refInfo[2] && window.App.model[refInfo[2]]) {
                                data[i] = window.App.model[refInfo[2]].find(refInfo[3]);
                            } else {
                                data[i] = null;
                            }
                        }
                    }
                    else if (typeof dataI === 'object') {
                        data[i] = resolveReference(dataI);
                    }
                }
            }
            return data;
        }

        function setReference(data) {
            var i,
                dataI,
                type;
            for (i in data) {
                dataI = data[i];
                type = dataI && dataI.get && dataI.get('type');
                if (type) {
                    data[i] = '#ref#' + type + '#' + dataI.get('id');
                }
                else if (typeof dataI === 'object') {
                    data[i] = setReference(dataI);
                }
            }
            return data;
        }

        BaseModel.prototype.create = function (attrs, success) {
            var ThisClass = this,
                object;
            attrs.type = ThisClass.namespace;
            attrs.flag = 'C';
            delete dataAll[this.namespace];
            delete dataAll.Workout;
            this.localStore(function (todos) {
                attrs.id = attrs.id || todos.length;
                todos[attrs.id] = attrs;
                return true;
            }, null, true);
            delete dataAll[this.namespace];
            delete dataAll.Workout;
            object = new ThisClass(attrs);
            if (success) {
                success(object);
            }
        };

        BaseModel.prototype.update = function (id, attrs, success) {
            var ThisClass = this,
                object;
            if (attrs.flag !== 'C') {
                attrs.flag = 'U';
            }
            delete dataAll[this.namespace];
            delete dataAll.Workout;
            this.localStore(function (objects) {
                var object = objects[id];
                for (var key in attrs) {
                    object[key] = attrs[key];
                }
                return true;
            }, null, true);
            delete dataAll[this.namespace];
            delete dataAll.Workout;

            object = new ThisClass(attrs);
            if (success) {
                success(object);
            }
        };

        BaseModel.prototype.destroy = function (id, success) {
            delete dataAll[this.namespace];
            delete dataAll.Workout;
            this.localStore(function (todos) {
                var todo = todos[id];
                todo.flag = 'D';
                return true;
            }, null, true);
            delete dataAll[this.namespace];
            delete dataAll.Workout;

            if (success) {
                success({});
            }
        };

        BaseModel.prototype.readFromForm = function (form) {
            var fields = this.fields,
                i,
                value,
                fieldsLength = fields.length,
                data = {},
                input,
                fieldName,
                fieldParts,
                k;
            for (i = 0; i < fieldsLength; i++) {
                k = 0;
                fieldName = fields[i].replace('%', k);
                input = form.querySelector('[name=' + fieldName + ']');
                while (input) {
                    value = input.value;
                    if (/%/.test(fields[i])) {
                        fieldParts = fields[i].split('-');
                        data[fieldParts[0]] = data[fieldParts[0]] || [];
                        data[fieldParts[0]][k] = data[fieldParts[0]][k] || {};
                        data[fieldParts[0]][k][fieldParts[2]] = value;
                        k++;
                        fieldName = fields[i].replace('%', k);
                        input = form.querySelector('[name=' + fieldName + ']');
                    }
                    else {
                        data[fields[i]] = value;
                        input = null;
                    }
                }
            }
            return data;
        };

        BaseModel.prototype.fillForm = function (form, data) {
            var fields = this.fields,
                i,
                j,
                value,
                fieldsLength = fields.length,
                input,
                fieldName,
                fieldParts,
                k,
                count;
            for (i = 0; i < fieldsLength; i++) {
                if (/%/.test(fields[i])) {
                    fieldParts = fields[i].split('-');
                    count = data && data.get(fieldParts[0]).length || 1;
                }
                else {
                    count = 1;
                }
                for (k = 0; k < count; k++) {
                    fieldName = fields[i].replace('%', k);
                    input = form.querySelector('[name=' + fieldName + ']');
                    if (input) {
                        fieldParts = fieldName.split('-');
                        value = data && data.get(fieldParts[0]) || '';
                        for (j = 1; value && j < fieldParts.length; j++) {
                            value = value[fieldParts[j]];
                        }
                        if (value && typeof value === 'object' && value.data) {
                            value = value.data.id;
                        }
                        input.value = value;
                    }
                }
            }
            return data;
        };

        BaseModel.prototype.fillByIds = function (prefix, data) {
            var fields = this.fields,
                i,
                j,
                value,
                fieldsLength = fields.length,
                text,
                fieldName,
                fieldParts,
                k,
                count;
            for (i = 0; i < fieldsLength; i++) {
                if (/%/.test(fields[i])) {
                    fieldParts = fields[i].split('-');
                    count = data && data.get(fieldParts[0]).length || 1;
                }
                else {
                    count = 1;
                }
                for (k = 0; k < count; k++) {
                    fieldName = fields[i].replace('%', k);
                    text = BaseWidget.find(prefix + fieldName);
                    if (text) {
                        fieldParts = fieldName.split('-');
                        value = data && data.get(fieldParts[0]) || '';
                        for (j = 1; value && j < fieldParts.length; j++) {
                            value = value[fieldParts[j]];
                        }
                        if (value) {
                            text.setValue(value);
                            text.show();
                        } else {
                            text.hide();
                        }
                    }
                }
            }
            return data;
        };

        BaseModel.clear = function () {
            dataAll = [];
        };

        model.Base = Base;
    }(document,
        window.Fw.model,
        window.Fw.model.Object,
        window.Fw.widget.Base
    ));
    //>>excludeStart('require', pragmas.require);
    return window.Fw.model.Base;
});
//>>excludeEnd('require');