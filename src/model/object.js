//>>excludeStart('require', pragmas.require);
define([
    "../model"
], function (model) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, model) {
        var ModelObject = function (Class, data, existing) {
            this.Class = Class;
            this.data = data || {};
            this.existing = existing;
        };

        ModelObject.prototype.get = function (field) {
            if (this.data) {
                if (field !== undefined) {
                    if (this[field] && this[field].get) {
                        return this[field].get(this.data);
                    }
                    return this.data[field];
                }
                return this.data;
            }
            return null;
        };

        ModelObject.prototype.set = function (field, value) {
                this.data[field] = value;

        };

        ModelObject.prototype.update = function (data) {
            var i;
            for (i in data) {
                this.set(i, data[i]);
            }
        };

        ModelObject.prototype.save = function (callback) {
            if (this.existing) {
                this.Class.update(this.data.id, this.data, callback);
            }
            else {
                this.Class.create(this.data, callback);
            }
        };

        ModelObject.prototype.destroy = function (callback) {
            this.Class.destroy(this.id, callback);
        };

        model.Object = ModelObject;
    }(document, window.Fw.model));
    //>>excludeStart('require', pragmas.require);
    return model.Object;
});
//>>excludeEnd('require');
