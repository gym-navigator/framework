(function (Fw) {
    "use strict";
//>>excludeStart('require', pragmas.require);
    define([
        "./core"
    ], function (Fw) {
        //>>excludeEnd('require');
        Fw.error = Fw.error || {};
        //>>excludeStart('require', pragmas.require);
        return Fw.error;
    });
//>>excludeEnd('require');
}(window.Fw));