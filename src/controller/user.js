//>>excludeStart('require', pragmas.require);
define([
    "../controller"
], function (controller) {
    "use strict";
    //>>excludeEnd('require');
    (function (document, controller) {
        controller.user = controller.user || {};
    }(document, window.Fw.controller));
    //>>excludeStart('require', pragmas.require);
    return controller.user;
});
//>>excludeEnd('require');
